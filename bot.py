from random import choice
import telebot
import printbbb

TOKEN = "6231785535:AAFaLoiXHFqe_W9IIVtu_iIHwJagFaXFeJg"

users = {}
"""
Словарь с пользователями.
Ключ: id пользователя - int
Значение:   - ответ на текущий вопрос - int
            - статус марафон/не марафон - bool
            - правильные ответы в марафоне - int
"""

TASKS = (printbbb.gen_easy, printbbb.third_file_from_np, printbbb.gen1, printbbb.gen2)
"""Все типы заданий"""

bot = telebot.TeleBot(TOKEN)

"""Клавиатура для выбора режима:"""
KEYBOARD = telebot.types.InlineKeyboardMarkup()
start = telebot.types.InlineKeyboardButton(text="Старт", callback_data="start")
marathon = telebot.types.InlineKeyboardButton(text="Марафон", callback_data="marathon")
KEYBOARD.row(start, marathon)


def give_task(this_guy: int):
    """Отправить вопрос на умножение с округлением, ответ в оперативке"""
    task, ans = choice(TASKS).__call__()
    users[this_guy][0] = str(ans)
    bot.send_message(this_guy, task)


def check_answer(message: telebot.types.Message):
    """Отреагировать на ответ или остановку сессии в стандартном режиме, продолжить тренировку"""
    this_guy = message.from_user.id
    if users[this_guy][0] == message.text:
        bot.send_message(this_guy, "Молодец!!!")
    elif message.text == "Стоп" or message.text == "стоп":
        users[this_guy] = None
        bot.send_message(this_guy, "Занятие остановлено")
    else:
        bot.send_message(this_guy, "Неправильно((")
    if users[this_guy]:
        give_task(this_guy)


def check_answer_marathon(message: telebot.types.Message):
    """Отреагировать на ответ или остановку сессии в режиме марафона, продолжить испытание"""
    this_guy = message.from_user.id
    if users[this_guy][0] == message.text:
        bot.send_message(this_guy, "Молодец!!!")
        users[this_guy][2] += int(1)
        give_task(this_guy)
    elif message.text == "Стоп" or message.text == "стоп":
        bot.send_message(this_guy, "Занятие остановлено, ваш результат: " + str(users[this_guy][2]))
        users[this_guy] = None
    else:
        bot.send_message(this_guy, "Неправильно(( ваш результат: " + str(users[this_guy][2]))
        users[this_guy] = None
    if users[this_guy] is None:
        bot.send_message(this_guy, "Выбери режим:", reply_markup=KEYBOARD)


def greet(message: telebot.types.Message):
    """Поздороваться"""
    keyboard = telebot.types.InlineKeyboardMarkup()
    start = telebot.types.InlineKeyboardButton(text="Старт", callback_data="start")
    marathon = telebot.types.InlineKeyboardButton(text="Марафон", callback_data="marathon")
    keyboard.row(start, marathon)
    with open("introduction.txt", "r", encoding="utf-8") as intro:
        bot.send_message(message.from_user.id, intro.read(), reply_markup=KEYBOARD)


@bot.callback_query_handler(func=lambda call: True)
def callback_function1(callback_obj: telebot.types.CallbackQuery):
    '''Установка режима по команде через клавиатуру'''
    this_guy = callback_obj.from_user.id
    if callback_obj.data == "start":
        users[this_guy] = [None, False, int(0)]
        give_task(this_guy)
    elif callback_obj.data == "marathon":
        users[this_guy] = [None, True, int(0)]
        give_task(this_guy)
    bot.answer_callback_query(callback_query_id=callback_obj.id)


@bot.message_handler(content_types=["text"])
def get_messages(message: telebot.types.Message):
    '''Выбор подходящей реакции на сообщение'''
    this_guy = message.from_user.id
    if users.get(this_guy) is None:
        greet(message)
    else:
        if users[this_guy][1]:
            check_answer_marathon(message)
        else:
            check_answer(message)


if __name__ == "__main__":
    bot.infinity_polling()
