from random import randint, randrange, choice, sample
from primerov import primers
from trigonometria import cool_sin, cool_cos, cool_tg, cool_ctg

# Вспомогательные функции:

def gen_num_of_4():
    '''возвращает список из 25 целых чисел, состоящих из 4 цифр, кратных 4'''
    s_of_num = []
    for _ in range(25):
        num = ''
        for _ in range(4):
            num += str(choice([4, 8]))
        s_of_num.append(num)
    return s_of_num


def gen_num_2():
    '''Возвращает список из 250 дробных чисел, очевидно кратных 2'''
    s_num = []
    for _ in range(250):
        cel = randrange(102, 200, 4)
        drob = randrange(102, 200, 4)
        s_num.append(cel + drob / 10 ** len(str(drob)))
    return s_num


def pprint(primer):
    if '+' in primer:
        t = '+'
    elif '-' in primer:
        t = '-'
    elif '*' in primer:
        t = '*'
    else:
        t = ':'
    return primer.split(t)[0] + ' ' + t + ' ' + primer.split(t)[1] + ' = ?'

# Основные функции:

def hitrie_primeri():
    # Примеры на умножение, обращающие некруглое число в круглое, пример: 350*2=700; 125*56=7000
    q = randint(1, 3)
    '''Один из трех вариантов первого числа в примере'''
    if q == 1:
        n1 = choice([125, 0.125, 12.5, 1.25])
        n2 = int(randrange(8, 100, 8))
    elif q == 2:
        n1 = 0.25
        n2 = int(choice(list(range(4, 100, 4)) + gen_num_of_4()))
    else:
        n1 = 0.5
        n2 = float(choice(list(range(2, 500, 2)) + gen_num_2()))
    '''Убираем незначащие нули после точки'''
    if n1 * n2 == int(n1 * n2):
        rez = int(n1 * n2)
    else:
        rez = n1 * n2
    if n1 == int(n1):
        n1 = int(n1)
    if n2 == int(n2):
        n2 = int(n2)
    primer = str(n1) + ' * ' + str(n2) + ' =  ?'
    return primer, rez


def easy_simple():
    # Примеры из файла
    r = randrange(0, 382)
    primer = primers[r]
    primer = primer.replace('х', '*')
    primer1 = primer.replace(':', '/')
    rez = int(eval(primer1))
    return pprint(primer), rez


def iz_skobok():
    # 1) (a + b) * c - a * c
    # Для быстрого решения примера необходимо раскрыть скобки
    a = randrange(201, 1000, 17)
    b = randrange(10, 100, 10)
    c = randrange(5, 12)
    primer = '(' + str(a) + ' + ' + str(b) + ') * ' + \
        str(c) + ' - ' + str(a) + ' * ' + str(c) + ' = ?'
    res = b * c
    return primer, res


def v_skobki():
    # 2) a * b + b * c, где a + c = 10, 100 или 1000
    # Для быстрого решения примера необходимо вынести
    # общий множитель за скобки
    b = randrange(201, 1000, 17)
    a = randrange(2, 778)
    c = 10 ** len(str(a)) - a
    primer = str(a) + ' * ' + str(b) + ' + ' + str(b) + ' * ' + str(c) + ' = ?'
    res = (a + c) * b
    return primer, res


def trigonometria():
    # Вся устная тригонометрия. Возвращается пример и четыре
    # варианта ответа, где первый правильный. Необходимо запомнить
    # правильный ответ, перемешать список и вывести на 4 кнопки
    all_angles = [0, 30, 45, 60, 90, 120, 135, 150, 180, 210, 225, 240, 270, 300, 315, 330, 360]
    name_func = ['sin', 'cos', 'tg', 'ctg']
    all_vars = ['не существует', '0', '1', '-1', '1/2', '-1/2', '1/√2', '-1/√2', '1/√3', '-1/√3', '√3/2', '-√3/2']
    func = choice(name_func)
    angle = choice(all_angles)
    if func == 'sin':
        return 'sin(' + str(angle) + ')  = ?', [cool_sin[str(angle)], sample(all_vars, 3)]
    elif func == 'cos':
        return 'cos(' + str(angle) + ')  = ?', [cool_cos[str(angle)], sample(all_vars, 3)]
    elif func == 'tg':
        return 'tg(' + str(angle) + ')  = ?', [cool_tg[str(angle)], sample(all_vars, 3)]
    else:
        return 'ctg(' + str(angle) + ')  = ?', [cool_ctg[str(angle)], sample(all_vars, 3)]

